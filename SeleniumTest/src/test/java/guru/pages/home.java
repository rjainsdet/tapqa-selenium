package guru.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class home
{
    private WebDriver driver;

    public home() {
    }

    @FindBy(xpath = "//a[@class='close']")
    public WebElement close;

    private static By menu = By.xpath("//ul[@id='java_technologies']");
    private static By list = By.xpath("//li[@class='fa fa-chevron-circle-right']");
    private static By tutorialLink = By.xpath("//a[text()='Live Selenium Project']");
    private static By emailinput = By.xpath("//input[@class='text']");
    private static By submit = By.xpath("//input[@alt='Submit Form']");


    public void launchWebDriver() {
        System. setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver=new ChromeDriver();
        driver.manage().window().maximize();
    }

    public void loadurl(String url) {
        driver.get(url);
    }

    public void waitForSeconds(int seconds) {
        try{
            Thread.sleep(seconds*1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void closeBrowser () {
        driver.close();
    }

    public void clickTutorialLink() {
        driver.findElement(tutorialLink).click();
    }

    public void enteremail(String email) {
        driver.findElement(emailinput).sendKeys(email);
        waitForSeconds(2);
        driver.findElement(submit).click();
    }

    public void printListOfElementsUnderSap() {
        WebElement sapmenu = driver.findElements(menu).get(3);
        List<WebElement> listundersap = sapmenu.findElements(list);
        for(int i=0;i<listundersap.size();i++){
            System.out.println("List Text "+(i+1)+": "+listundersap.get(i).getText());
        }
    }
}
