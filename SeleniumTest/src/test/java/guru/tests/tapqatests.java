package guru.tests;

import guru.pages.home;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;

public class tapqatests {

    private home home = new home();
    private Random random = new Random();

    public tapqatests() {
    }
    private static String guruUrl = "https://www.guru99.com/Home";

    @Before
    public void launchwebbrowser() {

        home.launchWebDriver();
    }

    @After
    public void closebrowser() {

        home.closeBrowser();
    }

    @Test
    public void testone() {
        home.loadurl(guruUrl);
        home.waitForSeconds(3);
        home.printListOfElementsUnderSap();
    }

    @Test
    public void signupfortutorial() {
        home.loadurl(guruUrl);
        home.waitForSeconds(3);
        home.clickTutorialLink();
        home.waitForSeconds(5);
        home.enteremail("rj"+ random.nextInt(10000)+"@gmail.com");
        home.waitForSeconds(5);
    }
}
